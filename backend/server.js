const { request, response } = require('express')
const express = require('express')

const db = require('./db')
const utils = require('./utils')

const app = express()

app.use(express.json())

app.get('/practice/test1', (request, response)=>{
  console.log(`inside test1 api`)
  response.send(`test1 successful`)
})

app.post('/practice/add', (request, response)=> {
  console.log(`inside add emp api`)
  const {name, salary, age} = request.body

  const connection = db.openDBConnection()

  const statement = `insert into EMP(name,salary,age) values('${name}','${salary}','${age}')`

  connection.query(statement, (error,result)=>{
    connection.end()
    if(error){
      response.send(utils.createResult(error))
    } else {
      response.send(utils.createResult(error,result))
    }
  })
})

app.get('/practice/getall', (request, response)=> {
  console.log(`inside get all emp api`)

  const connection = db.openDBConnection()

  const statement = `select * from EMP`

  connection.query(statement, (error,result)=>{
    connection.end()
    if(error){
      response.send(utils.createResult(error))
    } else {
      response.send(utils.createResult(error,result))
    }
  })
})

app.put('/practice/updateSalary', (request, response)=> {
  console.log(`inside update salary emp api`)
  const {id,salary} = request.body

  const connection = db.openDBConnection()

  const statement = `update EMP set salary='${salary}' where empid='${id}'`

  connection.query(statement, (error,result)=>{
    connection.end()
    if(error){
      response.send(utils.createResult(error))
    } else {
      response.send(utils.createResult(error,result))
    }
  })
})

app.get('/practice/getEmp/:id', (request, response)=> {
  console.log(`inside get emp details api`)
  const {id} = request.params

  const connection = db.openDBConnection()

  const statement = `select * from EMP where empid='${id}'`

  connection.query(statement, (error,result)=>{
    connection.end()
    if(error){
      response.send(utils.createResult(error))
    } else {
      response.send(utils.createResult(error,result))
    }
  })
})

app.delete('/practice/deleteEmp/:id', (request, response)=> {
  console.log(`inside get emp details api`)
  const {id} = request.params

  const connection = db.openDBConnection()

  const statement = `delete from EMP where empid='${id}'`

  connection.query(statement, (error,result)=>{
    connection.end()
    if(error){
      response.send(utils.createResult(error))
    } else {
      response.send(utils.createResult(error,result))
    }
  })
})

app.listen(4000, ()=>{
  console.log(`server started on port 4000`)
})
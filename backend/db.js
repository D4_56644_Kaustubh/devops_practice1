const mysql = require('mysql2')

const openDBConnection = () => {
  console.log(`inside open connection db.js`)
  const connection = mysql.createConnection({
    uri: "mysql://mysql_db:3306",
    user: "root",
    password: "manager",
    database: "mydb"
    // host: 'localhost',
    // port: 3306,
    // user: 'root',
    // password: 'manager',
    // database: 'mydb'
  })

  connection.connect()

  return connection
}

module.exports = {openDBConnection}
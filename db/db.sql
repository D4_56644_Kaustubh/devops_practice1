CREATE TABLE EMP (
  empid INTEGER primary key auto_increment,
  name VARCHAR(50),
  salary FLOAT,
  age INTEGER
);